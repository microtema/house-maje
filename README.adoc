= House Maje

:imagesdir: ./docs

:numbered:

include::docs/01_introduction_and_goals.adoc[]

include::docs/02_architecture_constraints.adoc[]

include::docs/03_system_scope_and_context.adoc[]

include::docs/04_solution_strategy.adoc[]

include::docs/05_building_block_view.adoc[]

include::docs/06_runtime_view.adoc[]

include::docs/07_deployment_view.adoc[]

include::docs/08_concepts.adoc[]

include::docs/09_architecture_decisions.adoc[]

include::docs/10_quality_requirements.adoc[]

include::docs/11_technical_risks.adoc[]

include::docs/12_glossary.adoc[]

